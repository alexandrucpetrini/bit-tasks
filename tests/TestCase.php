<?php

namespace Tests;

use App\Models\Task;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use WithFaker; // we get Faker with this trait
    protected $task_status_values = [
        Task::STATUS_PENDING,
        Task::STATUS_DOING,
        Task::STATUS_DONE,
    ];

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
    }
}

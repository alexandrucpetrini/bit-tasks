<?php

namespace Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class TaskSetStatusCommandTest extends TestCase
{
    /**
     * @test for: 'php artisan task:set-status' on valid id,
     *
     * @return void
     */
    public function testSetStatusCommandWorksOnValidIdYesBranch()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $random_task_status = $this->faker->randomElement($this->task_status_values);
        $this->artisan('task:set-status')
            ->expectsQuestion("What task do you want to change status? (id)", $random_task_id)
            ->expectsQuestion("What is the task status?", $random_task_status)
            ->expectsOutput("Task #{$random_task_id} status changed.")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:set-status' on invalid id
     *
     * @return void
     */
    public function testSetStatusCommandShowsErrorMessageOnInvalidId()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand($tasks_max_id + 1, $tasks_max_id + $tasks_max_id);
        $this->artisan('task:set-status')
            ->expectsQuestion("What task do you want to change status? (id)", $random_task_id)
            ->expectsOutput("Task #{$random_task_id} not found!")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:set-status' on missing 'tasks' table
     *
     * @return void
     */
    public function testSetStatusCommandThrowsQueryExceptionOnNoTable()
    {
        $this->artisan('migrate:reset');
        $this->expectException(QueryException::class);
        $this->artisan('task:set-status');
    }
}

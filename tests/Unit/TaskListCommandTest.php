<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Database\QueryException;

class TaskListCommandTest extends TestCase
{
    /**
     * @test for: 'php artisan task:list' on empty table
     *
     * @return void
     */
    public function testListCommandworksOnEmptyTable()
    {
        $this->artisan('migrate:fresh');
        $this->artisan('task:list')->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:list' on seeded table
     *
     * @return void
     */
    public function testListCommandWorksOnSeededTable()
    {
        $this->artisan('migrate:fresh --seed');
        $this->artisan('task:list')->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:list' on missing 'tasks' table
     *
     * @return void
     */
    public function testListCommandThrowsQueryExceptionOnNoTable()
    {
        $this->artisan('migrate:reset');
        $this->expectException(QueryException::class);
        $this->artisan('task:list');
    }
}

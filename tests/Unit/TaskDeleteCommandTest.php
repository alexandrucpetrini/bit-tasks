<?php

namespace Tests\Unit;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class TaskDeleteCommandTest extends TestCase
{
    /**
     * @test for: 'php artisan task:delete' on valid id,
     *  [yes] branch
     *
     * @return void
     */
    public function testDeleteCommandWorksOnValidIdYesBranch()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $this->artisan('task:delete')
            ->expectsQuestion("What task do you want to delete? (id)", $random_task_id)
            ->expectsQuestion("Are you sure you want to delete #{$random_task_id}?", true) // [yes]
            ->expectsOutput("Task #{$random_task_id} was deleted.")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:delete' on valid id,
     *  [no] branch
     *
     * @return void
     */
    public function testDeleteCommandWorksOnValidIdNoBranch()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $this->artisan('task:delete')
            ->expectsQuestion("What task do you want to delete? (id)", $random_task_id)
            ->expectsQuestion("Are you sure you want to delete #{$random_task_id}?", false) // [no]
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:delete' on invalid Id
     *
     * @return void
     */
    public function testDeleteCommandShowsErrorMessageOnInvalidId()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand($tasks_max_id + 1, $tasks_max_id + $tasks_max_id);
        $this->artisan('task:delete')
            ->expectsQuestion("What task do you want to delete? (id)", $random_task_id)
            ->expectsOutput("Task #{$random_task_id} not found!")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:delete' on missing 'tasks' table
     *
     * @return void
     */
    public function testDeleteCommandThrowsQueryExceptionOnNoTable()
    {
        $this->artisan('migrate:reset');
        $this->expectException(QueryException::class);
        $this->artisan('task:delete');
    }
}

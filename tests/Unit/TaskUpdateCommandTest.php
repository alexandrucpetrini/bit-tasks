<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class TaskUpdateCommandTest extends TestCase
{
    /**
     * @test for: 'php artisan task:update' on valid input,
     *  [no] branch
     *
     * @return void
     */
    public function testUpdateCommandWorksOnValidInputNoYesBranch()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $random_task_name = $this->faker->sentence(rand(1, 10));
        $this->artisan('task:update')
            ->expectsQuestion("What task do you want to update? (id)", $random_task_id)
            ->expectsQuestion("What is the new task name?", $random_task_name)
            ->expectsQuestion("Do you want to update task status?", false) // [no]
            ->expectsOutput("Task #{$random_task_id} was updated.")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:update' on valid input,
     *  [yes] branch
     *
     * @return void
     */
    public function testUpdateCommandWorksOnValidInputYesYesBranch()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $random_task_status = $this->faker->randomElement($this->task_status_values);
        $random_task_name = $this->faker->sentence(rand(1, 10));
        $this->artisan('task:update')
            ->expectsQuestion("What task do you want to update? (id)", $random_task_id)
            ->expectsQuestion("What is the new task name?", $random_task_name)
            ->expectsQuestion("Do you want to update task status?", true) // [yes]
            ->expectsQuestion("What is the task status?", $random_task_status)
            ->expectsOutput("Task #{$random_task_id} was updated.")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:update' on invalid id
     *
     * @return void
     */
    public function testUpdateCommandShowsErrorMessageOnInvalidId()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand($tasks_max_id + 1, $tasks_max_id + $tasks_max_id);
        $this->artisan('task:update')
            ->expectsQuestion("What task do you want to update? (id)", $random_task_id)
            ->expectsOutput("Task #{$random_task_id} not found!")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:update' on NULL name and no task status update
     *
     * @return void
     */
    public function testUpdateCommandThrowsQueryExceptionOnNullNameNoStatusUpdate()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $null_name = null;
        $this->expectException(QueryException::class);
        $this->artisan('task:update')
            ->expectsQuestion("What task do you want to update? (id)", $random_task_id)
            ->expectsQuestion("What is the new task name?", $null_name)
            ->expectsQuestion("Do you want to update task status?", false); // [no]
    }

    /**
     * @test for: 'php artisan task:update' on NULL name on task status update
     *
     * @return void
     */
    public function testUpdateCommandThrowsQueryExceptionOnNullNameWithStatusUpdate()
    {
        $this->artisan('migrate:fresh --seed');
        $tasks = DB::table('tasks')->get($columns = ['id']);
        $tasks_max_id = $tasks->last()->id;
        $random_task_id = rand(1, $tasks_max_id);
        $random_task_status = $this->faker->randomElement($this->task_status_values);
        $null_name = null;
        $this->expectException(QueryException::class);
        $this->artisan('task:update')
            ->expectsQuestion("What task do you want to update? (id)", $random_task_id)
            ->expectsQuestion("What is the new task name?", $null_name)
            ->expectsQuestion("Do you want to update task status?", true) // [yes]
            ->expectsQuestion("What is the task status?", $random_task_status);
    }

    /**
     * @test for: 'php artisan task:update' on missing 'tasks' table
     *
     * @return void
     */
    public function testUpdateCommandThrowsQueryExceptionOnNoTable()
    {
        $this->artisan('migrate:reset');
        $this->expectException(QueryException::class);
        $this->artisan('task:update');
    }
}

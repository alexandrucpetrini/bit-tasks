<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Database\QueryException;

class TaskCreateCommandTest extends TestCase
{
    /**
     * @test for: 'php artisan task:create' on valid input
     *
     * @return void
     */
    public function testCreateCommandWorksOnValidNameAndValidStatusOption()
    {
        $task_name = $this->faker->sentence(rand(1, 10));
        $task_status = $this->faker->randomElement($this->task_status_values);
        $this->artisan('migrate:fresh');
        $this->artisan('task:create')
            ->expectsQuestion("What is the task name?", $task_name)
            ->expectsQuestion("What is the task status?", $task_status) // needs actual status
            ->expectsOutput("Task {$task_name} was saved.")
            ->assertExitCode($exitCode = 0);
    }

    /**
     * @test for: 'php artisan task:create' on missing 'tasks' table
     *
     * @return void
     */
    public function testCreateCommandThrowsQueryExceptionOnNoTable()
    {
        $this->artisan('migrate:reset');
        $this->expectException(QueryException::class);
        $this->artisan('task:create');
    }

    /**
     * @test for: 'php artisan task:create' on NULL name
     *
     * @return void
     */
    public function testCreateCommandThrowsQueryExceptionOnNullName()
    {
        $task_name = null;
        $task_status = $this->faker->randomElement($this->task_status_values);
        $this->artisan('migrate:fresh');
        $this->expectException(QueryException::class);
        $this->artisan('task:create')
            ->expectsQuestion("What is the task name?", $task_name)
            ->expectsQuestion("What is the task status?", $task_status);
    }
}

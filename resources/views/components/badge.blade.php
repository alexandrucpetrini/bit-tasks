<div class="p-2 mb-1 {{ $badge->class }} text-uppercase font-weight-bold text-light">
    <span class="badge badge-light">{{ $badge->count }}</span>{{ $badge->type }}</div>

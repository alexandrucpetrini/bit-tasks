<div class="card border-light m-0">
    <div class="card-body">
        <h5 class="card-title">{{ ucfirst($task->name) }}</h5>
        <h6 class="card-subtitle mb-2 text-muted">
            Created at {{ $task->created_at->format("d/m/y") }} by {{ $task->owner }}
        </h6>
        <p class="card-text">{{ $task->description  }}</p>
    </div>
</div>

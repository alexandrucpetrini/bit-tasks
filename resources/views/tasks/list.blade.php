@extends('layouts.app')

@section('title', 'Page Title')
@section('content')
    <div class="row">
        <div class="row p3">
            @foreach ($types as $type => $tasks)
                @switch($type)
                    @case($status["STATUS_PENDING"])
                        @php
                            $badgeStatusColorClass = "bg-primary";
                            $type = "TODO";
                        @endphp
                        @break
                    @case($status["STATUS_DOING"])
                        @php $badgeStatusColorClass = "bg-warning"; @endphp
                        @break
                    @case($status["STATUS_DONE"])
                    @default
                        @php $badgeStatusColorClass = "bg-success"; @endphp
                @endswitch

                <div class="col-sm px-1">
                    @component('components.badge', [
                        "badge" => (object) [
                            "class" => $badgeStatusColorClass,
                            "count" => count($tasks),
                            "type" => $type
                        ]
                    ])
                    @endcomponent

                    @foreach ($tasks as $task)
                        @component('components.card', [ "task" => $task ]) @endcomponent
                    @endforeach
                </div>
            @endforeach
        </div>
    </div>
@endsection

<!DOCTYPE html>
<html>

    <head>
        <title>Tasks 1.0 - @yield('title')</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    </head>

    <body>
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand" href="#">
                <i class="fab fa-stack-overflow"></i>Tasks 1.0</a>
        </nav>
        <div class="container">@yield('content')</div>
        <footer>
            <link rel="stylesheet"
                  href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
                  integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
                  crossorigin="anonymous">
            <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                    crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"
                    integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o"
                    crossorigin="anonymous"></script>
        </footer>
    </body>

</html>

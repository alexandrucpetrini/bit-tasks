## **Task App #5**

### Important links

* **routes**
    * [web.php](routes/web.php)
* **controllers**
    * [TaskController.php](app/Http/Controllers/TaskController.php)
* **views**
    * **views/layouts**
        * [app.blade.php](resources/views/layouts/app.blade.php)
    * **views/tasks**
        * [list.blade.php](resources/views/tasks/list.blade.php)
    * **views/components**
        * [badge.blade.php](resources/views/components/badge.blade.php)
        * [card.blade.php](resources/views/components/card.blade.php)
* **migrations**
    * [create_tasks_table](database/migrations/2019_04_01_182954_create_tasks_table.php)
    * [extend_tasks_data](database/migrations/2019_05_22_192234_extend_tasks_data.php)
* **factories**
    * [TaskFactory.php](database/factories/TaskFactory.php)
* **seeds**
    * [DatabaseSeeder.php](database/seeds/DatabaseSeeder.php)
    * [TasksTableSeeder.php](database/seeds/TasksTableSeeder.php)
    * [ExtendTasksTableSeeder.php](database/seeds/ExtendTasksTableSeeder.php)
* **fixes for Task App #4**
    * Removed 'Are you sure you want to update ...?' from TaskUpdate class and from corresponding unit test
        * [TaskUpdate](app/Console/Commands/TaskUpdate.php)
        * [TaskUpdateCommandTest](tests/Unit/TaskUpdateCommandTest.php)
    * Removed 'Are you sure you want to change status ...?' from TaskSetStatus class and from corresponding unit test 
        * [TaskSetStatus](app/Console/Commands/TaskSetStatus.php)
        * [TaskSetStatusCommandTest](tests/Unit/TaskSetStatusCommandTest.php)
* **commits**
    * [commits](../../commits/all)

### Final result

* Browser screenshots

![Final Result Image 1](screenshots/TaskApp5/FinalResult1.png)
![Final Result image 2](screenshots/TaskApp5/FinalResult2.png)

* Database screenshot from [Sequeler](https://github.com/Alecaddd/sequeler)

![Database Look](screenshots/TaskApp5/DatabaseInSequeler.png)

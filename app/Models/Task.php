<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    const STATUS_PENDING = "PENDING";
    const STATUS_DOING = "DOING";
    const STATUS_DONE = "DONE";

    protected $fillable = [
        'name',
        'status',
    ];
}
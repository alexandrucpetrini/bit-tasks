<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;

class TaskList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows the user to list all present tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $tasks = Task::all()->toArray();
        $this->table(['id', 'name', 'status', 'created_at', 'updated_at'], $tasks);
    }
}

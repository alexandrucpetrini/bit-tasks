<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TaskSetStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:set-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows the user to change the status of an existing task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tasks = Task::all()->toArray();
        $this->table(['id', 'name', 'status', 'created_at', 'updated_at'], $tasks);
        $task_id = (int) $this->ask("What task do you want to change status? (id)");

        try {
            $task = Task::findOrFail($task_id);
            $task_status = $this->getStatus();

            $task->status = $task_status;
            $task->save();
            $this->info("Task #{$task->id} status changed.");

        } catch (ModelNotFoundException $e) {
            $this->error("Task #{$task_id} not found!");
        }
    }

    /**
     * Ask for the new status and return it
     *
     * @return string
     */
    private function getStatus($default = null)
    {
        $options = [
            Task::STATUS_PENDING,
            Task::STATUS_DOING,
            Task::STATUS_DONE,
        ];

        $task_status = $this->choice('What is the task status?', $options, $default);

        return $task_status;
    }
}

<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TaskDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows the user to delete a task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $tasks = Task::all()->toArray();
        $this->table(['id', 'name', 'status', 'created_at', 'updated_at'], $tasks);

        $task_id = (int) $this->ask("What task do you want to delete? (id)");

        try {
            $task = Task::findOrFail($task_id);

            if ($this->confirm("Are you sure you want to delete #{$task->id}?", $default = true)) {
                $task->delete();
                $this->info("Task #{$task->id} was deleted.");
            }
        } catch (ModelNotFoundException $e) {
            $this->error("Task #{$task_id} not found!");
        }
    }
}

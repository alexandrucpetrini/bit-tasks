<?php

namespace App\Console\Commands;

use App\Models\Task;
use Illuminate\Console\Command;

class TaskCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows the user to create a new task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        // query database to throw exception
        // when table and/or database does not exist
        $tasks = Task::all();

        $task_name = $this->ask('What is the task name?');
        $task_status = $this->getStatus($default = 0);

        $task = new Task;
        $task->name = $task_name;
        $task->status = $task_status;

        $task->save();

        $this->info("Task {$task->name} was saved.");
    }

    /**
     * Ask for the new status and return it
     *
     * @return string
     */
    private function getStatus($default = null)
    {
        $options = [
            Task::STATUS_PENDING,
            Task::STATUS_DOING,
            Task::STATUS_DONE,
        ];

        $task_status = $this->choice('What is the task status?', $options, $default);

        return $task_status;
    }
}

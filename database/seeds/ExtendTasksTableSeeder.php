<?php

use App\Models\Task;
use Illuminate\Database\Seeder;

class ExtendTasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tasks = Task::all();
        $tasks->each(function ($task) {
            $faker = Faker\Factory::create();
            $task->update(
                [
                    'description' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
                    // 'owner' => "{$faker->firstName()} {$faker->lastName()}", // Full name
                    'owner' => $faker->firstName(),
                    'importance' => rand(1, 10),
                ]
            );
        });
    }
}

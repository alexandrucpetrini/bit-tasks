<?php

use App\Models\Task;
use Faker\Generator as Faker;

$factory->define(App\Models\Task::class, function (Faker $faker) {
    $task_status_options = [
        Task::STATUS_PENDING,
        Task::STATUS_DOING,
        task::STATUS_DONE,
    ];
    return [
        'name' => $faker->bs(), // Todo real frases
        'status' => $faker->randomElement($task_status_options),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendTasksData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (blueprint $table) {
            $table->text('description')->nullable();
            $table->string('owner')->nullable();
            $table->integer('importance')->nullable();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $collumns = ['description', 'owner', 'importance', 'deleted_at'];

        /**
         * Drop columns only if they exist.
         * Fixes 'php artisan migrate:reset' and rollback errors.
         */
        foreach ($collumns as $collumn) {
            if ('deleted_at' == $collumn) {
                Schema::table('tasks', function (blueprint $table) {
                    $table->dropSoftDeletes();
                });
                continue;
            }
            if (Schema::hasColumn('tasks', $collumn)) {
                Schema::table('tasks', function (blueprint $table) use ($collumn) {
                    /**
                     * Seems we need 'doctrine/dbal'
                     * https://github.com/rappasoft/laravel-5-boilerplate/issues/785
                     */
                    $table->dropColumn($collumn);
                });
            }
        }
    }
}
